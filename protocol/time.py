from time import time

class Timer:
    def __init__(self) -> None:
        self.time = 0
        self.start = None
        self.depth = 0

    def __enter__(self, *args):
        if self.depth == 0:
            self.depth += 1
            self.start = time()

    def __exit__(self, *args):
        self.depth -= 1
        if self.depth == 0:
            self.time += time() - self.start
            self.start = None


class TimerManager:
    def __init__(self) -> None:
        self.__timer_by_fn = {}
        self.__timers_by_object = {}
    
    def server_time(self, object):
        return sum([ timer.time for timer in self.__timers_by_object[object] ])

    def object_functions_time(self, object):
        return {
            fn: timer.time
            for (obj, fn), timer in self.__timer_by_fn.items()
            if obj == object
        }
    
    def time_by_server(self): return self.__timers_by_object
    def servers(self): return self.__timers_by_object.keys()


    def time(self):
        def decorator(fn):
            def caller(obj, *args, **kwargs): 
                
                if obj not in self.__timers_by_object:
                    self.__timers_by_object[obj] = []

                if (obj, fn) not in self.__timer_by_fn:
                    timer = Timer()
                    self.__timer_by_fn[(obj, fn)] = timer
                    self.__timers_by_object[obj].append(timer)

                # perform the timing
                with self.__timer_by_fn[(obj, fn)]:
                    return fn( obj, *args, **kwargs )
            return caller
        return decorator
