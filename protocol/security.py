import json
from os import urandom
from typing import Union
from protocol.io import TextIOFileWrapper
from pipe import select
from phe import generate_paillier_keypair, paillier
from cryptography.hazmat.primitives.ciphers.aead import AESGCM


class Cipher: 
    """Cipher object handle the encryption/decryption process for a given cipher.
    """
    def gen_key(self) : 
        """Returns a new key."""
        pass
    def encrypt(self, key, plaintext):
        """Returns the given plaintext encrypted under the given key."""
        pass
    def decrypt(self, key, ciphertext): 
        """Returns the plaintext computed from the given ciphertext encrypted under the given key."""
        pass

class CipherCache:
    """CipherCache allows to speed up the key generation process by loading/saving a given key.
    
    The provided key is related to an unique identifier `id` used in the key store processing.
    """
    def save_key(self, id, key): 
        """Saves the key in the cache."""
        pass
    def load_key(self, id):
        """Loads the key from the cache."""
        pass
    def exists(self, id) -> bool:
        """Returns True if the a key related with the given id is exists in the cache, False otherwise."""
        pass 



class SecurityManager:
    def __init__(self) -> None:
        self.keys = {}
        self.__enabled = True
        self.__disabled_ciphers = set()
        self.__cipher_by_id = {}
        self.__cache_by_id = {}

    def add_cipher( self, id, cipher : Cipher, cache : CipherCache = None ):
        self.__cipher_by_id[id] = cipher
        if cache is not None:
            self.__cache_by_id[id] = cache
        self.__register_key( id=id, cipher=cipher, cache=cache )

    def add_paillier( self, id ):
        self.add_cipher( id=id, cipher=PaillierCipher(), cache=PaillierCache() )

    def encrypt( self, id , cipher : Cipher = None, plaintext = None, cache = None ):
        if cipher is None:
            if id in self.__cipher_by_id:
                cipher = self.__cipher_by_id[id]
            else:
                raise Exception(f"Undefined cipher for id {id}")
        
        if cache is None and id in self.__cache_by_id:
            cache = self.__cache_by_id[id]

        # instantiate the object with the se
        self.__register_key( cipher, id, cache = cache )


        # if message provided, encrypt it
        if plaintext is not None:
            if self.__enabled and id not in self.__disabled_ciphers:
                key = self.keys[id]
                return cipher.encrypt(key, plaintext)
            else:
                return plaintext
       
        def decorator( fn ):
            def call( obj, *args, **kwargs ):
                res = fn(obj, *args, **kwargs)
                assert res is not None, f"Encryption function cannot be applied over None (produced by {obj}.{fn.__name__})"
                if self.__enabled and id not in self.__disabled_ciphers:
                    key = self.keys[id]
                    return cipher.encrypt( key, res )
                else:
                    return res
            return call
        return decorator

    def decrypt( self, id, cipher : Cipher = None, ciphertext = None, cache = None ):
        if cipher is None:
            if id in self.__cipher_by_id:
                cipher = self.__cipher_by_id[id]
            else:
                raise Exception(f"Undefined cipher for id {id}")
        
        if cache is None and id in self.__cache_by_id:
            cache = self.__cache_by_id[id]


        # instantiate the object with the se
        self.__register_key( cipher, id, cache = cache )

        # if message provided, decrypt it
        if ciphertext is not None:
            if self.__enabled and id not in self.__disabled_ciphers:
                key = self.keys[id]
                return cipher.decrypt(key, ciphertext)
            else:
                return ciphertext

        def decorator( fn ):
            def call( obj, ciphertext ):
                if self.__enabled and id not in self.__disabled_ciphers:
                    key = self.keys[id]
                    plaintext = cipher.decrypt( key, ciphertext )
                else:
                    plaintext = ciphertext
                fn(obj, plaintext)
            return call
        return decorator

    def __register_key( self, cipher : Cipher, id : str, cache : Union[None, CipherCache] ):
        if id not in self.keys:
            if cache is not None:
                if cache.exists(id):
                    self.keys[id] = cache.load_key(id)
                else:
                    key = cipher.gen_key()
                    cache.save_key(id, key)
                    self.keys[id] = key
            else:
                key = cipher.gen_key()
                self.keys[id] = key
                
    def disable(self, *args):
        if args == ():
            self.__enabled = False
        else:
            for cipher_id in args:
                self.__disabled_ciphers.add(cipher_id)



class PaillierCipher(Cipher):
    def gen_key(self) : 
        return generate_paillier_keypair()
    def encrypt(self, key, plaintext): 
        (pk, sk) = key
        return pk.encrypt(plaintext)
    def decrypt(self, key, ciphertext): 
        (pk, sk) = key
        return sk.decrypt(ciphertext)

class PaillierCache(CipherCache):
    def exists(self, id): 
        file = TextIOFileWrapper( f"/tmp/paillier/{id}" )
        return file.exists()

    def save_key(self, id, key): 
        (pk, sk) = key
        file = TextIOFileWrapper( f"/tmp/paillier/{id}" )
        file.write(f"{pk.n},{sk.p},{sk.q}")
        
    def load_key(self, id): 
        file = TextIOFileWrapper( f"/tmp/paillier/{id}" )
        line = file.read()
        n, p, q = line.split(",") | select( lambda v: int(v) )
        pk = paillier.PaillierPublicKey( n )
        sk = paillier.PaillierPrivateKey( pk, p, q )
        return (pk, sk)


class AESGCMCipher(Cipher):
    def gen_key(self) : 
        return AESGCM.generate_key(256)
    def encrypt(self, key, plaintext): 
        cipher = AESGCM( key )
        encoded_plaintext = json.dumps(plaintext).encode()
        nonce = urandom(128)
        return nonce, cipher.encrypt( nonce=nonce, data=encoded_plaintext, associated_data=b"" )
    def decrypt(self, key, ciphertext): 
        cipher = AESGCM( key )
        nonce, ciphertext = ciphertext
        encoded_plaintext = cipher.decrypt( nonce=nonce, data=ciphertext, associated_data=b"" )
        return json.loads(encoded_plaintext.decode())
