.. GitLab Pages with Sphinx documentation master file, created by
   sphinx-quickstart on Thu Jan  9 10:28:38 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Official Python3 Protocol Library Documentation
====================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   _wiki/get_started.rst
   _wiki/examples.rst
   _apidocs/protocol.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
