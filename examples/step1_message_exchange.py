##################################################
# Author: Gael Marcadet <gael.marcadet@uca.fr>
# Description: Simple message exchange built with
#   the protocol library
##################################################
from protocol import Protocol, Server
import protocol
from protocol.security import SecurityManager, AESGCMCipher

# -----------------------------------------------
# SecurityManager object handles all the security
#   components of the protocol.
# -----------------------------------------------
security = SecurityManager()

# --------------------------------------------------
# Alice sends a message to Bob using AES-GCM cipher.
# --------------------------------------------------
class Alice(Server):
    def __init__(self): super().__init__(id="alice")

    @security.encrypt( id="secure", cipher=AESGCMCipher() )
    def send( self ):
        return "Hello !"

class Bob(Server):
    def __init__(self): super().__init__(id="bob")

    @security.decrypt( id="secure", cipher=AESGCMCipher() )
    def receive( self, message ):
        print(f"Alice said \"{message}\"")


# ------------------------------------------------
# Once we have defined what does the server, we
# define how they must interact.
# ------------------------------------------------
alice = Alice()
bob = Bob()
protocol = Protocol()
protocol.one_to_one( alice.send, bob.receive )

# ------------------------------------------------
# Notice that, thanks to the SecurityManager, you
#   can disable all security features in a single
#   line (uncomment the next one to do it).
# ------------------------------------------------
# security.disable()


# ------------------------------------------------
# We then play the message exchange protocol.
# ------------------------------------------------
protocol.play()

# ------------------------------------------------
# Using the Protocol object, you can see packets
#   transmitted over the network.
# ------------------------------------------------
for source, destination, message in protocol.packets():
    print(f"{source} -> {destination} : {message}")