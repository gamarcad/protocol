##################################################
# Author: Gael Marcadet <gael.marcadet@uca.fr>
# Description: Architecture used to perform
#   a sum of integers.
##################################################
from protocol import Protocol, Server
from protocol.security import PaillierCache, SecurityManager, PaillierCipher, AESGCMCipher
from protocol.time import TimerManager

# -----------------------------------------------
# SecurityManager object handles all the security
#   components of the protocol. 
#
#   We will change the way we define the security
#   by setting up security description here.
#   Using this way, you can change the used cipher
#   by modifying a single line of code.
#
#   Note the usage of `cache` parameter that allows
#   to cache the key. This method allows to speed up
#   the key generation and management process.
# -----------------------------------------------
security = SecurityManager()
security.add_cipher( id="secure", cipher=PaillierCipher(), cache=PaillierCache() )
# security.add_cipher( id="secure", cipher=AESGCMCipher() )

# -----------------------------------------------
# TimeManager object handles the time measurement
#   of all executed functions.
# -----------------------------------------------
timer = TimerManager()

# --------------------------------------------------
# DataOwner server owns an integer that we want to
#   use to perform the sum.
# --------------------------------------------------
class DataOwner(Server):
    def __init__(self, index): 
        super().__init__(id=f"do{index}")

    @timer.time()
    @security.encrypt( id="secure" )
    def send( self ):
        return 1

# --------------------------------------------------
# Client performs the sum.
# --------------------------------------------------
class Client(Server):
    def __init__(self):
        super().__init__(id="client")
        self.sum = 0

    @timer.time()
    @security.decrypt( id="secure" )
    def receive( self, score ):
        self.sum += score
    
    def say_the_sum(self):
        print(f"The sum is {self.sum}")


# ------------------------------------------------
# Once we have defined what does the server, we
# define how they must interact.
# ------------------------------------------------
dois = [ DataOwner(index) for index in range(10) ] 
client = Client()
protocol = Protocol()

# First, each data owner sends his score to the client;
for data_owner in dois:
    protocol.one_to_one( data_owner.send, client.receive )

# Then, the client prints the score in the standar output.
protocol.process( client.say_the_sum )

# ------------------------------------------------
# Notice that, thanks to the SecurityManager, you
#   can disable all security features in a single
#   line (uncomment the next one to do it).
# ------------------------------------------------
# security.disable()


# ------------------------------------------------
# We then play the message exchange protocol.
# ------------------------------------------------
protocol.play()

# ------------------------------------------------
# Using the Protocol object, you can see packets
#   transmitted over the network.
# ------------------------------------------------
print("=" * 10, "Exchanged Messages", "=" * 10)
for source, destination, message in protocol.packets():
    print(f"{source} -> {destination} : {message}")

# ------------------------------------------------
# Using the TimerManager object, you can see the 
#   execution time of each server.
# ------------------------------------------------
print("=" * 10, "Execution Time", "=" * 10)
for server in timer.servers():
    server_execution_time = timer.server_time(server) 
    print(f"{server} : {server_execution_time}s")