#!/usr/bin/env python
from distutils.core import setup

REQUIREMENTS_FILENAME="requirements.txt"
def read_requirements():
    """Reads and returns the requirements."""
    with open(REQUIREMENTS_FILENAME, 'r') as file:
        return file.readlines()

setup(
    name='protocol',
    version='1.0',
    description='Protocol builder for quick experiment',
    author='Gael Marcadet',
    author_email='gael.marcadet@uca.fr',
    url='https://gitlab.com/gamarcad/protocol',
    install_requires=read_requirements(),
)