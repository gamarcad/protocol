class OneToOne:
    def __init__(self,a, b):
        self.a, self.b = a, b
    
    def play( self):
        self.b(self.a())

class ManyToOne:
    def __init__(self,sources, destination):
        self.sources = sources
        self.destination = destination
    
    def play( self):
        self.destination([ source() for source in self.sources ])

class InteractionType:
    PROCESS = 1
    ONE_TO_ONE = 2

class Protocol:
    def __init__(self): 
        self.interactions = []
        self.__packets = []
    
    def one_to_one(self, from_server_fn, to_server_fn):
        self.interactions.append(( InteractionType.ONE_TO_ONE, from_server_fn, to_server_fn))
    
    def process(self, server_fn ):
        self.interactions.append(( InteractionType.PROCESS, server_fn ))

    def play(self): 
        for (interaction_type, *functions) in self.interactions:
            if interaction_type == InteractionType.PROCESS:
                server_fn = functions[0]
                server_fn()
            
            elif interaction_type == InteractionType.ONE_TO_ONE:
                from_server_fn, to_server_fn = functions

                # perform the interaction
                data = from_server_fn()
                to_server_fn(data)

                # capture the interaction
                from_server : Server = from_server_fn.__self__
                to_server : Server = to_server_fn.__self__
                self.__packets.append((from_server, to_server, data))
            else:
                raise Exception(f"Unknown interaction type {interaction_type}")
            

    def packets(self): return self.__packets

    def times(self): 
        return {
            server.id: server.time()
            for server in []
        }

class Server: 
    def __init__(self, id): 
        self.id = id
    
    def __str__(self) -> str:
        return self.id